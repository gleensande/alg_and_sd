#include <iostream>
#include <cassert>

#define LEFT -1
#define MIDDLE 0
#define RIGHT 1

using std::cout;
using std::cin;
using std::endl;
using std::max;

class BinaryTreeNode {
 private:
	int value_;
	BinaryTreeNode* left_;
	BinaryTreeNode* right_;
 public:
	BinaryTreeNode(int value = 0) : value_(value), left_(NULL), right_(NULL) {}
	int getValue();
	BinaryTreeNode* getLeft();
	BinaryTreeNode* getRight();
	void setLeft(BinaryTreeNode* node);
	void setRight(BinaryTreeNode* node);
	void print();
};

void BinaryTreeNode::print() {
	cout << value_ << ' ';
}

int BinaryTreeNode::getValue() {
	return value_; 
}

BinaryTreeNode* BinaryTreeNode::getLeft() { 
	return left_; 
}

BinaryTreeNode* BinaryTreeNode::getRight() {
	return right_; 
}

void BinaryTreeNode::setLeft(BinaryTreeNode* node) { 
	left_ = node; 
}

void BinaryTreeNode::setRight(BinaryTreeNode* node) { 
	right_ = node; 
}

class BinaryTree {
 private:
	BinaryTreeNode* root_;
 public:
	BinaryTree(int value);
	bool add(int value);
	void walk();
	~BinaryTree(); 
};

BinaryTree::BinaryTree(int value) { 
	root_ = new BinaryTreeNode(value);
}

bool BinaryTree::add(int value) {
	BinaryTreeNode* curParent = NULL;
	BinaryTreeNode* current = root_;
	int where = MIDDLE;
	if (root_ != NULL) {
		while ( current != NULL ) {
			if (value >= current->getValue()) {
				curParent = current;
				current = current->getRight();
				where = RIGHT;
			} else {
				curParent = current;
				current = current->getLeft();
				where = LEFT;
			}
		}
	}
	if (current == NULL) {
		if (where == LEFT) {
			BinaryTreeNode* nodeNew = new BinaryTreeNode(value);
			curParent->setLeft(nodeNew);
			return true;
		} else if (where == RIGHT) {
			BinaryTreeNode* nodeNew = new BinaryTreeNode(value);
			curParent->setRight(nodeNew);
			return true;
		} else {
			root_ = new BinaryTreeNode(value);
			return true;
		}
	}
	return false;
}

int defaultInitialSize = 16;

class Stack {
 private:
	BinaryTreeNode** buffer;
	int bufferSize;
	int realSize;
	void grow();
 public:
	Stack() : buffer(NULL), bufferSize(0), realSize(0) {}
	~Stack() { delete[] buffer; }
	int size() const { return realSize; }
	void push(BinaryTreeNode* element);
	BinaryTreeNode* pop();
	BinaryTreeNode* top();
	bool isEmpty() const { return (realSize == 0 ? 1 : 0);}
	void print() const;
};

void Stack::grow() {
	int newBufferSize = max(2 * bufferSize, defaultInitialSize);
	BinaryTreeNode** newBuffer = new BinaryTreeNode*[newBufferSize];
	for (int i = 0; i < realSize; ++i) {
		newBuffer[i] = buffer[i];
	}
	delete[] buffer;
	buffer = newBuffer;
	bufferSize = newBufferSize;
}

void Stack::push(BinaryTreeNode* element) {
	if(bufferSize == realSize) {
		grow();
	}
	assert(realSize < bufferSize && buffer != 0);
	buffer[realSize] = element;
	realSize++;
}

BinaryTreeNode* Stack::pop() {
	if(isEmpty()) {
		return NULL;
	}
	realSize--;
	return buffer[realSize];
}

BinaryTreeNode* Stack::top() {
	if(isEmpty()) {
		return NULL;
	}
	return buffer[realSize - 1];
}

void Stack::print() const {
	if (bufferSize == 0 || buffer == 0 || realSize == 0) {
		return;
	}
	for (int i = 0; i < realSize; i++) {
		buffer[i]->print();
	}
	cout << endl;
}

void BinaryTree::walk() {
	if (root_ == NULL) {
		return;
	}

	Stack s;
	BinaryTreeNode* prev = NULL;
	s.push(root_);

	while (!s.isEmpty()) {
		BinaryTreeNode* curr = s.top();
		if (!prev || prev->getLeft() == curr || prev->getRight() == curr) {
			if (curr->getLeft() != NULL) {
				s.push(curr->getLeft());
			} else if (curr->getRight() != NULL) {
				s.push(curr->getRight());
			} else  {
				curr->print();
				s.pop();
			}
		} else if (curr->getLeft() == prev) {
			if (curr->getRight() != NULL) {
				s.push(curr->getRight());
			} else {
				curr->print();
				s.pop();
			}
		} else if (curr->getRight() == prev) {
			curr->print();
			s.pop();
		}
		prev = curr;
	}
}

BinaryTree::~BinaryTree() {
	if (root_ == NULL) {
		return;
	}

	Stack s;
	BinaryTreeNode* prev = NULL;
	s.push(root_);

	while (!s.isEmpty()) {
		BinaryTreeNode* curr = s.top();
		if (!prev || prev->getLeft() == curr || prev->getRight() == curr) {
			if (curr->getLeft() != NULL) {
				s.push(curr->getLeft());
			} else if (curr->getRight() != NULL) {
				s.push(curr->getRight());
			} else  {
				delete s.pop();
			}
		} else if (curr->getLeft() == prev) {
			if (curr->getRight() != NULL) {
				s.push(curr->getRight());
			} else {
				delete s.pop();
			}
		} else if (curr->getRight() == prev) {
			delete s.pop();
		}
		prev = curr;
	}
}

int main() {
	int N;
	cin >> N;

	int value;
	cin >> value;
	
	BinaryTree BinTree(value);

	for (int i = 1; i < N; i++) {
		cin >> value;
		BinTree.add(value);
	}	
	BinTree.walk();
	cout << endl;
}
