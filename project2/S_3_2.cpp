#include <cassert>
#include <iostream>

#define LEFT -1
#define MIDDLE 0
#define RIGHT 1

using std::cout;
using std::cin;
using std::endl;
using std::max;

int defaultInitialSize = 16;

class DecartTreeNode {
 private:
  int key_;
  int priority_;
  int level_;
  DecartTreeNode* left_;
  DecartTreeNode* right_;

 public:
  DecartTreeNode(int key = 0, int priority = 0, int level = 0)
      : key_(key),
        priority_(priority),
        level_(level),
        left_(NULL),
        right_(NULL) {}
  DecartTreeNode* getLeft() { return left_; }
  DecartTreeNode* getRight() { return right_; }
  DecartTreeNode** getLeftAddr() { return &left_; }
  DecartTreeNode** getRightAddr() { return &right_; }
  int getKey() { return key_; }
  void setKey(int key) { key_ = key; }
  void setLeft(DecartTreeNode* node) { left_ = node; }
  void setRight(DecartTreeNode* node) { right_ = node; }
  void print();
  int getLevel() { return level_; }
  int getPriority() { return priority_; }
  void setPriority(int priority) { priority_ = priority; }
};

class DecartTree {
 private:
  DecartTreeNode* root_;
  int maxLevel_;

 public:
  DecartTree(int key, int priority);
  bool add(int key, int priority);
  int maxLevelsValue();
  void countLevelsWidth();
  void split(DecartTreeNode* currentNode, int key, DecartTreeNode** left,
             DecartTreeNode** right);
  void print();
  int getWidth(DecartTreeNode* node, int level);
  int getMaxWidth();
  int height(DecartTreeNode* node);
  ~DecartTree();
};

DecartTree::DecartTree(int key, int priority) {
  root_ = new DecartTreeNode(key, priority);
  maxLevel_ = 0;
}

bool DecartTree::add(int key, int priority) {
  DecartTreeNode* curParent = NULL;
  DecartTreeNode* current = root_;
  int level = 0;
  int where = MIDDLE;
  while (current != NULL && current->getPriority() >= priority) {
    curParent = current;
    if (key >= current->getKey()) {
      current = current->getRight();
      where = RIGHT;
    } else {
      current = current->getLeft();
      where = LEFT;
    }
    level++;
    if (level > maxLevel_) {
      maxLevel_ = level;
    }
  }
  if (current == NULL) {
    DecartTreeNode* nodeNew = new DecartTreeNode(key, priority, level);
    if (where == LEFT) {
      curParent->setLeft(nodeNew);
    } else if (where == RIGHT) {
      curParent->setRight(nodeNew);
    } else {
      root_ = nodeNew;
    }
    return true;
  } else {
    DecartTreeNode* left;
    DecartTreeNode* right;
    split(current, key, &left, &right);
    DecartTreeNode* nodeNew = new DecartTreeNode(key, priority, level);
    if (where == LEFT) {
      curParent->setLeft(nodeNew);
    } else if (where == RIGHT) {
      curParent->setRight(nodeNew);
    } else {
      root_ = nodeNew;
    }
    nodeNew->setLeft(left);
    nodeNew->setRight(right);
  }
  return false;
}

void DecartTree::split(DecartTreeNode* currentNode, int key,
                       DecartTreeNode** left, DecartTreeNode** right) {
  if (currentNode == NULL) {
    *left = NULL;
    *right = NULL;
  } else if (currentNode->getKey() > key) {
    split(currentNode->getLeft(), key, left, currentNode->getLeftAddr());
    *right = currentNode;
  } else {
    split(currentNode->getRight(), key, currentNode->getRightAddr(), right);
    *left = currentNode;
  }
}

class Stack {
 private:
  DecartTreeNode** buffer;
  int bufferSize;
  int realSize;
  void grow();

 public:
  Stack() : buffer(NULL), bufferSize(0), realSize(0) {}
  ~Stack() { delete[] buffer; }
  int size() const { return realSize; }
  void push(DecartTreeNode* element);
  DecartTreeNode* pop();
  DecartTreeNode* top();
  bool isEmpty() const { return (realSize == 0 ? 1 : 0); }
  void print() const;
};

void Stack::grow() {
  int newBufferSize = max(2 * bufferSize, defaultInitialSize);
  DecartTreeNode** newBuffer = new DecartTreeNode*[newBufferSize];
  for (int i = 0; i < realSize; ++i) {
    newBuffer[i] = buffer[i];
  }
  delete[] buffer;
  buffer = newBuffer;
  bufferSize = newBufferSize;
}

void Stack::push(DecartTreeNode* element) {
  if (bufferSize == realSize) {
    grow();
  }
  assert(realSize < bufferSize && buffer != 0);
  buffer[realSize] = element;
  realSize++;
}

DecartTreeNode* Stack::pop() {
  if (isEmpty()) {
    return NULL;
  }
  realSize--;
  return buffer[realSize];
}

DecartTreeNode* Stack::top() {
  if (isEmpty()) {
    return NULL;
  }
  return buffer[realSize - 1];
}

void Stack::print() const {
  if (bufferSize == 0 || buffer == 0 || realSize == 0) {
    return;
  }
  for (int i = 0; i < realSize; i++) {
    buffer[i]->print();
  }
  cout << endl;
}

DecartTree::~DecartTree() {
  if (root_ == NULL) {
    return;
  }

  Stack s;
  DecartTreeNode* prev = NULL;
  s.push(root_);

  while (!s.isEmpty()) {
    DecartTreeNode* curr = s.top();
    if (!prev || prev->getLeft() == curr || prev->getRight() == curr) {
      if (curr->getLeft() != NULL) {
        s.push(curr->getLeft());
      } else if (curr->getRight() != NULL) {
        s.push(curr->getRight());
      } else {
        delete s.pop();
      }
    } else if (curr->getLeft() == prev) {
      if (curr->getRight() != NULL) {
        s.push(curr->getRight());
      } else {
        delete s.pop();
      }
    } else if (curr->getRight() == prev) {
      delete s.pop();
    }
    prev = curr;
  }
}

void DecartTree::print() {
  if (root_ == NULL) {
    return;
  }

  Stack s;
  DecartTreeNode* prev = NULL;
  s.push(root_);

  while (!s.isEmpty()) {
    DecartTreeNode* curr = s.top();
    if (!prev || prev->getLeft() == curr || prev->getRight() == curr) {
      if (curr->getLeft() != NULL) {
        s.push(curr->getLeft());
      } else if (curr->getRight() != NULL) {
        s.push(curr->getRight());
      } else {
        cout << curr->getKey() << ' ';
        s.pop();
      }
    } else if (curr->getLeft() == prev) {
      if (curr->getRight() != NULL) {
        s.push(curr->getRight());
      } else {
        cout << curr->getKey() << ' ';
        s.pop();
      }
    } else if (curr->getRight() == prev) {
      cout << curr->getKey() << ' ';
      s.pop();
    }
    prev = curr;
  }
}

int DecartTree::height(DecartTreeNode* node) {
  if (node == NULL) {
    return 0;
  }
  int left = height(node->getLeft());
  int right = height(node->getRight());

  return max(left, right) + 1;
}

int DecartTree::getMaxWidth() {
  int maxWidth = 0;
  int width = 0;
  int h = height(root_);

  for (int i = 1; i < h; i++) {
    width = getWidth(root_, i);
    if (width > maxWidth) {
      maxWidth = width;
    }
  }

  return maxWidth;
}

int DecartTree::getWidth(DecartTreeNode* node, int level) {
  if (node == NULL) {
    return 0;
  }
  if (level == 1) {
    return 1;
  } else if (level > 1) {
    return getWidth(node->getLeft(), level - 1) +
           getWidth(node->getRight(), level - 1);
  } else {
    return 0;
  }
}

void DecartTreeNode::print() {
  Stack s;
  DecartTreeNode* prev = NULL;
  s.push(this);

  while (!s.isEmpty()) {
    DecartTreeNode* curr = s.top();
    if (!prev || prev->getLeft() == curr || prev->getRight() == curr) {
      if (curr->getLeft() != NULL) {
        s.push(curr->getLeft());
      } else if (curr->getRight() != NULL) {
        s.push(curr->getRight());
      } else {
        cout << curr->getKey() << ' ';
        s.pop();
      }
    } else if (curr->getLeft() == prev) {
      if (curr->getRight() != NULL) {
        s.push(curr->getRight());
      } else {
        cout << curr->getKey() << ' ';
        s.pop();
      }
    } else if (curr->getRight() == prev) {
      cout << curr->getKey() << ' ';
      s.pop();
    }
    prev = curr;
  }
}

int main() {
  int N;
  cin >> N;

  int key, priority;
  cin >> key >> priority;

  DecartTree BinTree(key, 0);
  DecartTree DecTree(key, priority);

  for (int i = 1; i < N; i++) {
    cin >> key >> priority;
    BinTree.add(key, 0);
    DecTree.add(key, priority);
  }
  cout << DecTree.getMaxWidth() - BinTree.getMaxWidth() << endl;
}
