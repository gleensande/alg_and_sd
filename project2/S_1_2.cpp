#include <iostream>
#include <string>

#define defaultSize 8
#define hashConst1 17
#define hashConst2 19

#define EMPTY "$%empty%$"
#define DEL "$%del%$"

#define FREE -1

using std::cin;
using std::cout;
using std::string;
using std::endl;

class HashTable {
 private:
  int hashConsts[2];
  int size;
  int notEmpty;
  int realElements;
  string* array;
  float alpha;
  float beta;
  int HashAdditional(string line, int hashConstNum);

 public:
  HashTable();
  int Hash(string line, int probNum);
  void reHash();
  bool Has(string line);
  bool Add(string line);
  bool Remove(string line);
  ~HashTable();
  void print();
};

HashTable::HashTable() {  // ok
  size = defaultSize;
  notEmpty = 0;
  alpha = 0;
  hashConsts[0] = hashConst1;
  hashConsts[1] = hashConst2;
  array = new string[size];
  for (int i = 0; i < size; i++) {
    array[i] = EMPTY;
  }
  beta = 0;
  realElements = 0;
}

int HashTable::Hash(string line, int probNum) {  // ok
  int result =
      (HashAdditional(line, 0) + (probNum + 1) * HashAdditional(line, 1)) %
      size;
  // cout << "ProbNum: " << probNum << ", result = " << result << endl;
  return result;
}

int HashTable::HashAdditional(string line, int hashConstNum) {  // ok
  int result = (unsigned char)line[0];
  for (unsigned int i = 1; i < line.length(); i++) {
    result = (result * hashConsts[hashConstNum] + (unsigned char)line[i]);
  }
  result %= size;
  if (hashConstNum == 1 && result % 2 == 0) {
    result++;
  }
  return result;
}

void HashTable::reHash() {  // ok
  notEmpty = 0;
  alpha = 0;
  size *= 2;
  beta = 0;
  string* m = array;
  array = new string[size];
  for (int i = 0; i < size; i++) {
    array[i] = EMPTY;
  }
  for (int i = 0; i < size / 2; i++) {
    if (m[i] != EMPTY && m[i] != DEL) {
      Add(m[i]);
    }
  }
  delete[] m;
}

bool HashTable::Add(string line) {
  if (beta >= 0.75) {
    reHash();
  }
  int probNum = 0;
  int place = Hash(line, probNum);
  int neededPlace = FREE;
  while (array[place] != EMPTY && array[place] != line) {
    if (array[place] == DEL && neededPlace == FREE) {
      neededPlace = place;
    }
    probNum++;
    if (probNum == size) {
      if (neededPlace != FREE) {
        array[neededPlace] = line;
        alpha = (float)notEmpty / (float)size;
        realElements++;
        beta = (float)realElements / (float)size;
        return true;
      }
      return false;
    }
    place = Hash(line, probNum);
  }
  if (array[place] == line) {
    return false;
  } else {
    if (neededPlace == FREE) {
      neededPlace = place;
      notEmpty++;
    }
    array[neededPlace] = line;
    alpha = (float)notEmpty / (float)size;
    realElements++;
    beta = (float)realElements / (float)size;
    return true;
  }
}

bool HashTable::Remove(string line) {
  int probNum = 0;
  int place = Hash(line, probNum);
  while (array[place] != EMPTY && array[place] != line) {
    probNum++;
    if (probNum == size) {
      return false;
    }
    place = Hash(line, probNum);
  }
  if (array[place] == line) {
    array[place] = DEL;
    realElements--;
    beta = (float)realElements / (float)size;
    return true;
  } else {
    return false;
  }
}

bool HashTable::Has(string line) {
  int probNum = 0;
  int place = Hash(line, probNum);
  while (array[place] != EMPTY && array[place] != line) {
    probNum++;
    if (probNum == size) {
      return false;
    }
    place = Hash(line, probNum);
  }
  if (array[place] == line) {
    return true;
  } else {
    return false;
  }
}

HashTable::~HashTable() { delete[] array; }

void HashTable::print() {
  for (int i = 0; i < size; i++) {
    cout << '[' << array[i] << ']' << endl;
  }
}

int main() {
  HashTable table;

  char command;
  string line;
  while (cin >> command >> line) {
    switch (command) {
      case '+':
        cout << (table.Add(line) ? "OK" : "FAIL") << endl;
        break;
      case '-':
        cout << (table.Remove(line) ? "OK" : "FAIL") << endl;
        break;
      case '?':
        cout << (table.Has(line) ? "OK" : "FAIL") << endl;
        break;
      default:
        break;
    }
    // table.print();
  }

  return 0;
}
