#!/bin/bash

# INPUT:
# $1 - num of project
# $2 - num of problem
# $3 - num of var

if [ -n "$1" -a -n "$2" -a -n "$3" ]
then
	a=$1
	b=$2
	c=$3
else
	a=2
	b=1
	c=2
fi

echo "***** RUN compiling *****"
c++ project${a}/${b}_${c}.cpp -o ${a}_${b}_${c}.out -std=c++14 -g -O2 -Wall
echo ""
