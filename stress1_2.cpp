#include <iostream>
#include <string>
#include <unordered_set>

using std::cin;
using std::cout;
using std::string;
using std::endl;
using std::unordered_set;

int main() {
	unordered_set <string> table;
	
	char command;
	string line;
	while (cin >> command >> line) {
		switch (command) {
			case '+' : cout << (table.insert(line).second ? "OK" : "FAIL") << endl; break;
			case '-' : cout << (table.erase(line) ? "OK": "FAIL") << endl; break;
			case '?' : cout << (table.find(line) != table.end() ? "OK" : "FAIL") << endl; break;
			default: break;
		}
	}

	return 0;
}
