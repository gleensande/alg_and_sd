#include <iostream>
#include <cassert>

using std::cin;
using std::cout;
using std::endl;
using std::max;
using std::swap;

int defaultInitialSize = 16;

struct Train {
 private:
	int arrive;
	int depart;
 public:
	Train() : arrive(0), depart(0) {}
	Train(int _arrive, int _depart) : arrive(_arrive), depart(_depart) {}
	int getArrive() { return arrive; }
	int getDepart() { return depart; }
};

class DynamicArr {
 private:
	Train* buffer;
	int bufferSize;
	int realSize;
	void grow();
 public:
	DynamicArr() : buffer(0), bufferSize(0), realSize(0) {}
	~DynamicArr() { delete[] buffer; }
	int size() const { return realSize; }
	Train getAt(int index) const;
	void setAt(int i, Train element);
	Train operator[] (int index) const { return getAt(index); }
	void add(Train element);
	void removeLast();
	void print() const;
};

Train DynamicArr::getAt(int index) const {
	assert(index >= 0 && index < realSize && buffer != 0);
	return buffer[index];
}

void DynamicArr::setAt(int i, Train element) {
	assert(realSize <= bufferSize);
	assert(buffer != 0);
	buffer[i] = element;
}

void DynamicArr::grow() {
	int newBufferSize = max(2 * bufferSize, defaultInitialSize);
	Train* newBuffer = new Train[newBufferSize];
	for (int i = 0; i < realSize; ++i) {
		newBuffer[i] = buffer[i];
	}
	delete[] buffer;
	buffer = newBuffer;
	bufferSize = newBufferSize;
}

void DynamicArr::add(Train element) {
	if(bufferSize == realSize) {
		grow();
	}
	assert(realSize < bufferSize);
	assert(buffer != 0);
	buffer[realSize] = element;
	realSize++;
}

void DynamicArr::removeLast() {
	assert(realSize <= bufferSize);
	assert(buffer != 0);
	realSize--;
}

void DynamicArr::print() const {
	if (bufferSize == 0 || buffer == 0 || realSize == 0) {
		cout << "[empty]";
	}
	for (int i = 0; i < realSize; i++) {
		cout << "[" << buffer[i].getArrive() << ", " << buffer[i].getDepart() << "] ";
	}
	cout << endl;
}

class Heap {
 private:
	DynamicArr arr;
	void SiftUp(int i);
	void SiftDown(int i);
 public:
	void add(Train element);
	Train ExtractMax();
	Train viewMax() const { return (arr.getAt(0)); }
	int size() const { return (arr.size()); }
	void print() const { arr.print(); }
};

void Heap::SiftUp(int i) {
	if (i < size()) {
		while (i > 0) {
			Train element = arr.getAt(i);
			int parent_i = (i - 1) / 2;
			Train parent = arr.getAt(parent_i);
			Train swap;
			if (element.getDepart() < parent.getDepart()) {
				swap = element;
				arr.setAt(i, parent);
				arr.setAt(parent_i, swap);
				i = parent_i;
			} else {
				return;
			}
		}
	}
}

void Heap::SiftDown(int i) {
	while (2 * i + 1 < arr.size()) {
		int left = 2 * i + 1;
		int right = 2 * i + 2;
		int j = left;
		if (right < arr.size() && (arr.getAt(right).getDepart() < arr.getAt(left).getDepart())) {
			j = right;
		}
		if (arr.getAt(i).getDepart() <= arr.getAt(j).getDepart()) {
			break;
		}
		Train swap = arr.getAt(i);
		arr.setAt(i, arr.getAt(j));
		arr.setAt(j, swap);
		i = j;
	}
}

void Heap::add(Train element) {
	arr.add(element);
	if (arr.size() - 1 != 0) {
		SiftUp(arr.size() - 1);
	}
}

Train Heap::ExtractMax() {
	assert(arr.size() != 0);
	Train max = viewMax();
	Train last = arr.getAt(arr.size() - 1);
	arr.setAt(0, last);
	arr.removeLast();
	if (arr.size() != 0) {
		SiftDown(0);
	}
	return max;
}

int main() {
	int n;
	cin >> n;

	Heap heap;

	int arrive, depart;
	for (int i = 0; i < n; i++) {
		cin >> arrive >> depart;
		Train input(arrive, depart);
		if (heap.size() > 0) {
			if (input.getArrive() > heap.viewMax().getDepart()) {
				heap.ExtractMax();
			}
		}
		heap.add(input);
	}

	cout << heap.size() << endl;

	return 0;
}
