#include <iostream>
#include <cassert>
#include <cstring>

#define BIRTH 0
#define DEATH 1

using std::cin;
using std::cout;
using std::endl;
using std::max;
using std::swap;
using std::min;

int defaultInitialSize = 16;

struct Date {
 private:
	int day;
	int mon;
	int year;
	int persNum;
	int type;
 public:
	Date() : day(0), mon(0), year(0), persNum(-1), type(-1) {}
	Date(int _day, int _mon, int _year, int _num, int _type) : day(_day), mon(_mon), year(_year), persNum(_num), type(_type) {}
	int getDay() const { return day; }
	int getMon() const { return mon; }
	int getYear() const { return year; }
	int getType() const { return type; }
	Date changeType() { return Date(day, mon, year, persNum, !type); }
	bool operator> (Date d);
	bool operator< (Date d);
	bool operator== (Date d);
	Date operator+ (int add_year);
	Date operator- (int add_year);
	bool operator>= (Date d);
	bool operator<= (Date d);
	void print() const { cout << '[' << day << ' ' << mon << ' ' << year << " #" << persNum << " t:" << type << ']' << endl; }
};

bool Date::operator> (Date d) {
	if (year > d.getYear()) {
		return true;
	} else if (year == d.getYear()) {
		if (mon > d.getMon()) {
			return true;
		} else if (mon == d.getMon()) {
			if (day > d.getDay()) {
				return true;
			}
		}
	}

	return false;
}

bool Date::operator< (Date d) {
	if (year < d.getYear()) {
		return true;
	} else if (year == d.getYear()) {
		if (mon < d.getMon()) {
			return true;
		} else if (mon == d.getMon()) {
			if (day < d.getDay()) {
				return true;
			}
		}
	}

	return false;
}

bool Date::operator== (Date d) {
	if (day == d.getDay() && mon == d.getMon() && year == d.getYear()) {
		return true;
	}

	return false;
}

bool Date::operator>= (Date d) {
	if (*this > d || *this == d) {
		return true;
	}
	return false;
}

bool Date::operator<= (Date d) {
	if (*this < d || *this == d) {
		return true;
	}
	return false;
}

Date Date::operator+ (int add_year) {
	return Date(day, mon, year + add_year, persNum, type);
}
		
Date Date::operator- (int min_day) {
	return Date(day - min_day, mon, year, persNum, type);
}
		
class DynamicArr {
 private:
	Date* buffer;
	int bufferSize;
	int realSize;
	int count;
	int maxCount;
	void grow();
 public:
	DynamicArr() : buffer(0), bufferSize(0), realSize(0), count(0), maxCount(1) {}
	~DynamicArr() { delete[] buffer; }
	Date* getBuffer() { return buffer; }
	int size() const { return realSize; }
	Date getAt(int index) const;
	void setAt(int i, Date element);
	Date operator[] (int index) const { return getAt(index); }
	void add(Date element);
	void removeLast();
	int countAll();
	void print() const;
};

Date DynamicArr::getAt(int index) const {
	assert(index >= 0 && index < realSize && buffer != 0);
	return buffer[index];
}

void DynamicArr::setAt(int i, Date element) {
	assert(realSize <= bufferSize);
	assert(buffer != 0);
	buffer[i] = element;
}

void DynamicArr::grow() {
	int newBufferSize = max(2 * bufferSize, defaultInitialSize);
	Date* newBuffer = new Date[newBufferSize];
	for (int i = 0; i < realSize; ++i) {
		newBuffer[i] = buffer[i];
	}
	delete[] buffer;
	buffer = newBuffer;
	bufferSize = newBufferSize;
}

void Merge(Date* a, int firstLen, Date* b, int secondLen, Date* c) {
	int i = 0;
	int j = 0;
	int k = 0;
	while (i != firstLen && j != secondLen) {
		if (a[i] < b[j]) {
			c[k] = a[i];
			i++;
		} else if (a[i] > b[j]) {
			c[k] = b[j];
			j++;
		} else {
			if (a[i].getType() == DEATH) {
				c[k] = a[i];
				i++;
			} else {
				c[k] = b[j];
				j++;
			}
		}
					
		k++;
	}
	if (i == firstLen) {
		while (j != secondLen) {
			c[k] = b[j];
			k++;
			j++;
		}
	} else {
		while (i != firstLen) {
			c[k] = a[i];
			k++;
			i++;
		}
	}
}

void mergeSort(Date* a, int aLen) {
	if (aLen <= 1) {
		return;
	}
	int firstLen = aLen / 2;
	int secondLen = aLen - firstLen;
	mergeSort(a, firstLen);
	mergeSort(a + firstLen, secondLen);
	Date* c = new Date[aLen];
	Merge(a, firstLen, a + firstLen, secondLen, c);
	memcpy(a, c, sizeof(Date) * aLen);
	delete[] c;
}

void DynamicArr::add(Date element) {
	if(bufferSize == realSize) {
		grow();
	}
	assert(realSize < bufferSize);
	assert(buffer != 0);

	buffer[realSize] = element;
	realSize++;
}

void DynamicArr::removeLast() {
	assert(realSize <= bufferSize);
	assert(buffer != 0);
	realSize--;
}

int DynamicArr::countAll() {
	if (realSize == 0) {
		return 0;
	}
	for (int i = 0; i < realSize; i++) {
		if (buffer[i].getType() == BIRTH) {
			count++;
			if (count > maxCount) {
				maxCount = count;
			}
		} else {
			count--;
		}
	}
	return maxCount;	
}

void DynamicArr::print() const {
	if (bufferSize == 0 || buffer == 0 || realSize == 0) {
		cout << "[empty]";
	}
	for (int i = 0; i < realSize; i++) {
		buffer[i].print();
	}
	cout << endl;
}

int main() {
	int n;
	cin >> n;

	DynamicArr dates;

	int day, mon, year;
	for (int i = 0; i < n; i++) {
		cin >> day >> mon >> year;
		Date birth(day, mon, year, i, BIRTH);
		cin >> day >> mon >> year;
		Date death(day, mon, year, i, DEATH);
		if (death > birth + 18) {
			dates.add(birth + 18);
			if (death < birth + 80) {
				dates.add(death);
			} else {
				dates.add(birth.changeType() + 80);
			}
		}
	}

	mergeSort(dates.getBuffer(), dates.size());
	cout << dates.countAll() << endl;
}
