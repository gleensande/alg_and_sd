#include <iostream>
#include <cassert>
#include <cstring>

#define BYTE_SIZE 256
#define BITES 8

typedef unsigned long long Long;

using std::cin;
using std::cout;
using std::endl;
using std::max;

unsigned int defaultInitialSize = 16;

class DynamicArr {
 private:
	Long* buffer;
	unsigned int bufferSize;
	unsigned int realSize;
	void grow();
 public:
	DynamicArr() : buffer(0), bufferSize(0), realSize(0) {}
	~DynamicArr() { delete[] buffer; }
	unsigned int size() const { return realSize; }
	Long getAt(unsigned int index) const;
	Long operator[] (unsigned int index) const { return getAt(index); }
	void add(Long element);
	void LSDSort();
	void CountSort(unsigned int byteNum);
	void print() const;
};

Long DynamicArr::getAt(unsigned int index) const {
	assert(index < realSize && buffer != 0);
	return buffer[index];
}

void DynamicArr::grow() {
	unsigned int newBufferSize = max(2 * bufferSize, defaultInitialSize);
	Long* newBuffer = new Long[newBufferSize];
	for (unsigned int i = 0; i < realSize; ++i) {
		newBuffer[i] = buffer[i];
	}
	delete[] buffer;
	buffer = newBuffer;
	bufferSize = newBufferSize;
}

void DynamicArr::add(Long element) {
	if(bufferSize == realSize) {
		grow();
	}
	assert(realSize < bufferSize && buffer != 0);
	buffer[realSize] = element;
	realSize++;
}

void DynamicArr::LSDSort() {
	for (int i = 0; i < BITES; i++) {
		CountSort(i);
	}
}

void DynamicArr::CountSort(unsigned int byteNum) {
	Long k = BYTE_SIZE;
	Long* c = new Long[k];
	
	for (unsigned int i = 0; i < k; i++) {
		c[i] = 0;
	}

	Long shift2 = 8 * byteNum;

	unsigned int byte;
	for (unsigned int i = 0; i < realSize; i++) {
		byte = (buffer[i] >> shift2) & 0xff;
		++c[byte];
	}

	unsigned int sum = 0;
	for (unsigned int i = 0; i < k; i++) {
		unsigned int tmp = c[i];
		c[i] = sum;
		sum += tmp;
	}

	Long* b = new Long[realSize];
	for (unsigned int i = 0; i < realSize; i++) {
		byte = (buffer[i] >> shift2) & 0xff;
		b[c[byte]++] = buffer[i];
	}
	
	memcpy(buffer, b, realSize * sizeof(Long));

	delete[] c;
	delete[] b;
}

void DynamicArr::print() const {
	if (bufferSize == 0 || buffer == 0 || realSize == 0) {
		cout << "[empty]";
	}
	for (unsigned int i = 0; i < realSize; i++) {
		cout << buffer[i] << ' ';
	}
	cout << endl;
}

int main() {
	unsigned int n;
	cin >> n;

	DynamicArr numbers;

	Long inp;
	for (unsigned int i = 0; i < n; i++) {
		cin >> inp;
		numbers.add(inp);	
	}

	numbers.LSDSort();

	numbers.print();

	return 0;
}
