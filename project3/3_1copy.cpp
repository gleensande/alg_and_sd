#include <iostream>
#include <vector>
#include <climits>

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::min;
using std::set;
using std::pair;

class CMatrixGraph {
 public:
  explicit CMatrixGraph(size_t vertices_count);

  void AddEdge(int from, int to, int time);

  int VerticesCount() const { return matrix_.size(); }

  vector<int> GetNextVertices(int vertex) const;
  vector<int> GetNextNonConstVertices(int vertex) const;
  vector<int> GetPrevVertices(int vertex) const;
  vector<int> GetAllNonConstVertices() const;

  int Dijkstra(int from, int to);

 private:
  vector<vector<int>> matrix_;
  vector<int> mark_;
  vector<bool> is_mark_const_;
  set<pair<int, int> vertices_; // all nonconst points: mark and number
};

CMatrixGraph::CMatrixGraph(size_t vertices_count) {
  matrix_.resize(vertices_count);
  for (size_t i = 0; i < vertices_count; i++) {
    matrix_[i].resize(vertices_count);
  }
  mark_.resize(vertices_count);
  is_mark_const_.resize(vertices_count);
  for (size_t i = 0; i < vertices_count; i++) {
  	mark_[i] = INT_MAX;
 		is_mark_const_[i] = false;
  }
}

void CMatrixGraph::AddEdge(int from, int to, int time) {
  matrix_[from][to] = time;
  matrix_[to][from] = time;  // т.к. все указанные дороги двусторонние
}

vector<int> CMatrixGraph::GetNextVertices(int vertex) const {
  vector<int> next;
  for (size_t i = 0; i < matrix_.size(); i++) {
    if (matrix_[vertex][i] != 0 ) {
      next.push_back(i);
    }
  }
  return next;
}

vector<int> CMatrixGraph::GetNextNonConstVertices(int vertex) const {
  vector<int> allnext = GetNextVertices(vertex);
  vector<int> nonconst;
  for (int next : allnext) {
    if (is_mark_const_[next] == false) {
      nonconst.push_back(next);
    }
  }
  
  return nonconst;
}

vector<int> CMatrixGraph::GetAllNonConstVertices() const {
  vector<int> nonconst;
  
  for (size_t i = 0; i < is_mark_const_.size(); i++) {
    if (is_mark_const_[i] == false) {
      nonconst.push_back(i);
    }
  }

  return nonconst;
}


vector<int> CMatrixGraph::GetPrevVertices(int vertex) const {
  vector<int> prev;
  for (size_t i = 0; i < matrix_.size(); i++) {
    if (matrix_[i][vertex] != 0 ) {
      prev.push_back(i);
    }
  }
  return prev;
}

int CMatrixGraph::Dijkstra(int from, int to) {
  mark_[from] = 0;
  is_mark_const_[from] = true;
  int current = from;

  int min_val;
  int min_num;
  while(is_mark_const_[to] != true) {
    vector<int> neighbours = GetNextNonConstVertices(current);
    for (int i : neighbours) {
      mark_[i] = min(mark_[i], mark_[current] + matrix_[current][i]);
    }

    vector<int> allNonConst = GetAllNonConstVertices();
    
    min_val = mark_[allNonConst[0]];
    min_num = allNonConst[0];
    for (size_t i = 1; i < allNonConst.size(); i++) {
      if (mark_[allNonConst[i]] < min_val) {
        min_val = mark_[allNonConst[i]];
        min_num = allNonConst[i];
      }
    }

    is_mark_const_[min_num] = true;
    current = min_num;
  }  

  return mark_[to];
}

int main() {
  int N;
  cin >> N;

  int M;
  cin >> M;

  CMatrixGraph graph(N);
  int from, to, time;
  for (int i = 0; i < M; i++) {
    cin >> from >> to >> time;
    graph.AddEdge(from, to, time);
  }

  cin >> from >> to;
  cout << graph.Dijkstra(from, to) << endl;
}
