#include <iostream>
#include <vector>
#include <queue>
#include <climits>

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::queue;

class CListGraph {
 public:
    CListGraph(size_t vertices_count);
    void AddEdge(int v1, int v2);
    int VerticesCount() const { return edges_.size(); }
    vector<int> GetConnectedVertices(int vertex) const;
    int BFS(int start_vertex, int stop_vertex);
 private:
    vector<vector<int>> edges_;
};

CListGraph::CListGraph(size_t vertices_count)
     : edges_(vertices_count) {}

void CListGraph::AddEdge(int v1, int v2) {
    edges_[v1].push_back(v2);
    edges_[v2].push_back(v1);
}

vector<int> CListGraph::GetConnectedVertices(int vertex) const {
    vector<int> vertices = edges_[vertex];
    return vertices;
}

int CListGraph::BFS(int start_vertex, int stop_vertex) {
    queue<int> q;
    vector<bool> visited(VerticesCount(), false);    
    vector<int> s(VerticesCount(), INT_MAX);
    vector<int> p(VerticesCount(), 0);

    q.push(start_vertex);
    visited[start_vertex] = true;
    s[start_vertex] = 0;
    p[start_vertex] = 1;

    int current = start_vertex;
    while(!q.empty()) {
        current = q.front();
        q.pop();
        
        vector<int> neighbours = GetConnectedVertices(current);
        for (int neighbour : neighbours) {
            if (visited[neighbour] == false) {
                if (s[current] + 1 < s[neighbour]) {
                    p[neighbour] = p[current];
                    s[neighbour] = s[current] + 1;
                    q.push(neighbour);
                } else if (s[current] + 1 == s[neighbour]) {
                    p[neighbour] += p[current];
                }
            }
        }

        visited[current] = true;
    }
    return p[stop_vertex];
}

int main() {
    int v;
    cin >> v;

    int n;
    cin >> n;

    CListGraph graph(v);

    int v1, v2;
    for (int i = 0; i < n; i++) {
        cin >> v1 >> v2;
        graph.AddEdge(v1, v2);
    }

    int u, w;
    cin >> u >> w;
    cout << graph.BFS(u, w) << endl;

    return 0;
}
