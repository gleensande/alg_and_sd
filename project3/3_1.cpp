#include <iostream>
#include <vector>
#include <climits>
#include <set>

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::min;
using std::set;
using std::pair;
using std::make_pair;

struct CEdge {
  int from;
  int to;
  int time;
  CEdge(int _from, int _to, int _time) : from(_from), to(_to), time(_time) {}
};

class CGraph {
 public:
  explicit CGraph(size_t vertices_count);
  void AddEdge(int from, int to, int time);
  int VerticesCount() const { return vertices_count_; }
  vector<int> GetConnectedVertices(int vertex) const;
  int findMinTime(int from, int to);
  int Dijkstra(int from, int to);
 private:
  vector<CEdge> edges_;
  int vertices_count_;
};

CGraph::CGraph(size_t vertices_count)
     : vertices_count_(vertices_count) {}

void CGraph::AddEdge(int from, int to, int time) {
  edges_.push_back(CEdge(from, to, time));
}

vector<int> CGraph::GetConnectedVertices(int vertex) const {
  vector <int> vertices;
  
  for (CEdge c : edges_) {
    if (c.from == vertex) {
      vertices.push_back(c.to);
    } else if (c.to == vertex) {
      vertices.push_back(c.from);
    }
  }

  return vertices;
}

int CGraph::findMinTime(int from, int to) {
  int time = INT_MAX;
  for (CEdge c : edges_) {
    if (c.from == from && c.to == to) {
      if (c.time < time) {
        time = c.time;
      }
    }
  }
  return time;
}

int CGraph::Dijkstra(int from, int to) {
  set<pair<int, int> > q;  // time, vertex num  -- analog to priority queue
  vector<bool> visited(VerticesCount(), false);    
  vector<int> s(VerticesCount(), INT_MAX);  // sizes
  vector<int> p(VerticesCount(), -1);  // parent
  
  s[from] = 0;
  q.insert(make_pair(0, from));

  while(!q.empty())  {
    auto current = q.begin();
    q.erase(current);
    visited[current_num] = true;
  
    int current_num = (*current).second;
  
    vector<int> neighbours = GetConnectedVertices(current_num);
    for (int neighbour : neighbours) {
      if (visited[neighbour] == false) {
        int time = findMinTime(current_num, neighbour);
        if (s[neighbour] == INT_MAX) {
          s[neighbour] = s[current_num] + time;
          q.insert(make_pair(time, neighbour));
        } else if (s[neighbour] > s[current_num] + time) {
          q.erase(make_pair(s[neighbour], neighbour));
          s[neighbour] = s[current_num] + time;
          p[neighbour] = current_num;
          q.insert(make_pair(time, current_num));
        }
      }
    }
  }

  return s[to];
}

int main() {
  int N;
  cin >> N;

  int M;
  cin >> M;

  CGraph graph(N);
  int from, to, time;
  for (int i = 0; i < M; i++) {
    cin >> from >> to >> time;
    graph.AddEdge(from, to, time);
  }

  cin >> from >> to;
  cout << graph.Dijkstra(from, to) << endl;
}
