#include <iostream>
#include <cstdlib>
#include <ctime>

#include "clistgraph.hpp"
#include "cmatrixgraph.hpp"
#include "csetgraph.hpp"
#include "carcgraph.hpp"

using std::vector;
using std::pair;

using std::srand;
using std::rand;
using std::time;

using std::cin;
using std::cout;
using std::endl;

const int MAX_VERTICES_COUNT = 20;

vector<pair<int, int>> create_vector_of_rand_pairs(int size, int lt);
void do_something_with_graph(IGraph* graph, vector<pair<int, int>> edges);
void print_something_about_graph(IGraph* graph);

int main() {
  srand(time(NULL));
  int vertices_count = rand() % MAX_VERTICES_COUNT + 1;

  // edges
  vector<pair<int, int>> edges = create_vector_of_rand_pairs(vertices_count / 2, vertices_count);
  cout << "Randomly generated edges:" << endl;
  for (auto edge : edges) {
    cout << '(' << edge.first << ',' << edge.second << ')' << endl;
  }

  // CListGraph
  CListGraph graph1(vertices_count);
  do_something_with_graph(&graph1, edges);
  cout << "CListGraph:" << endl;
  print_something_about_graph(&graph1); 
 
  // CMatrixGraph 
  CMatrixGraph graph2(vertices_count);
  do_something_with_graph(&graph2, edges);
  cout << "CMatrixGraph:" << endl;
  print_something_about_graph(&graph2); 

  // copy CListGraph to CMatrixGraph
  CMatrixGraph graph3(&graph1);
  cout << "CMatrixGraph copied from CListGraph:" << endl;
  print_something_about_graph(&graph3); 

  // CSetGraph 
  CSetGraph graph4(vertices_count);
  do_something_with_graph(&graph4, edges);
  cout << "CSetGraph:" << endl;
  print_something_about_graph(&graph4); 

  // CArcGraph 
  CArcGraph graph5(vertices_count);
  do_something_with_graph(&graph5, edges);
  cout << "CArcGraph:" << endl;
  print_something_about_graph(&graph5); 

  return 0;
}

vector<pair<int, int>> create_vector_of_rand_pairs(int size, int lt) {
  vector<pair<int, int>> rand_pairs(size);

  for (int i = 0; i < size; i++) {
    rand_pairs[i].first = rand() % lt;
    rand_pairs[i].second = rand() % lt;
  }
  
  return rand_pairs;
}

void do_something_with_graph(IGraph* graph, vector<pair<int, int>> edges) {
  // add random edges
  for (auto edge : edges) {
  graph->AddEdge(edge.first, edge.second);
  }
}

void print_something_about_graph(IGraph* graph) {
  // vertices count
  cout << "Vertices count = " << graph->VerticesCount() << endl;

  // prev vertices
  cout << "Prev vertices: " << endl;
  for (int i = 0; i < graph->VerticesCount(); i++) {
    vector<int> prev_vertices = graph->GetPrevVertices(i);
    cout << i << ": ";
    for (int prev : prev_vertices) {
      cout << prev << ' ';
    }
    cout << endl;
  }

  // next vertices
  cout << "Next vertices: " << endl;
  for (int i = 0; i < graph->VerticesCount(); i++) {
    vector<int> next_vertices = graph->GetNextVertices(i);
    cout << i << ": ";
    for (int next : next_vertices) {
      cout << next << ' ';
    }
    cout << endl;
  }

  cout << endl;
}
