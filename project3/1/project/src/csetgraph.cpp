#include "csetgraph.hpp"

CSetGraph::CSetGraph(size_t vertices_count) 
  : array_(vertices_count) {}

CSetGraph::CSetGraph(IGraph* graph) {
  int vertices_count = graph->VerticesCount();

  array_.resize(vertices_count);
  
  for (int i = 0; i < vertices_count; i++) {
    std::vector<int> next_vertices = graph->GetNextVertices(i);
    for (int next : next_vertices) {
      AddEdge(i, next);
    }
  }
}

void CSetGraph::AddEdge(int from, int to) {
  array_[from].insert(to);
}

std::vector<int> CSetGraph::GetNextVertices(int vertex) const { 
  std::vector<int> next;
  for (auto element : array_[vertex]) {
    next.push_back(element);
  }
  return next;
}

std::vector<int> CSetGraph::GetPrevVertices(int vertex) const {
  std::vector<int> prev;
  for (int i = 0; i < VerticesCount(); i++) {
    if (array_[i].find(vertex) != array_[i].end()) {
      prev.push_back(i);
    }
  }
  return prev;
}
