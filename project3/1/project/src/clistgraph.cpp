#include "clistgraph.hpp"

#include <assert.h>
#include <iostream>

CListGraph::CListGraph(size_t vertices_count)
    : out_edges_(vertices_count),
      in_edges_(vertices_count) {}

CListGraph::CListGraph(IGraph* graph) {
  int vertices_count = graph->VerticesCount();

    out_edges_.resize(vertices_count);
    in_edges_.resize(vertices_count);
  
  for (int i = 0; i < vertices_count; i++) {
    std::vector<int> next_vertices = graph->GetNextVertices(i);
    for (int next : next_vertices) {
      AddEdge(i, next);
    }      
  }
}

void CListGraph::AddEdge(int from, int to) {
  assert(from >= 0 && from < VerticesCount());
  assert(to >= 0 && to < VerticesCount());

  // No checks (multigraph)
  out_edges_[from].push_back(to);
  in_edges_[to].push_back(from);
}

std::vector<int> CListGraph::GetNextVertices(int vertex) const {
  assert(vertex >= 0 && vertex < VerticesCount());
  std::vector<int> vertices = out_edges_[vertex];
  return vertices;
}

std::vector<int> CListGraph::GetPrevVertices(int vertex) const {
  assert(vertex >= 0 && vertex < VerticesCount());
  std::vector<int> vertices = in_edges_[vertex];
  return vertices;
}
