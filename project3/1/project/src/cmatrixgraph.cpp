#include "cmatrixgraph.hpp"

CMatrixGraph::CMatrixGraph(size_t vertices_count) {
  matrix_.resize(vertices_count);
  for ( int i = 0 ; i < vertices_count ; i++ ) {
    matrix_[i].resize(vertices_count);
  }
}

CMatrixGraph::CMatrixGraph(IGraph* graph) {
  int vertices_count = graph->VerticesCount();

  matrix_.resize(vertices_count);
  for ( int i = 0 ; i < vertices_count ; i++ ) {
    matrix_[i].resize(vertices_count);
  }
  
  for (int i = 0; i < vertices_count; i++) {
    std::vector<int> next_vertices = graph->GetNextVertices(i);
    for (int next : next_vertices) {
      AddEdge(i, next);
    }      
  }
}

void CMatrixGraph::AddEdge(int from, int to) {
  matrix_[from][to] = true;
}

std::vector<int> CMatrixGraph::GetNextVertices(int vertex) const {
  std::vector<int> next;
  for (int i = 0; i < matrix_.size(); i++) {
    if (matrix_[vertex][i] == true ) {
      next.push_back(i);
    }
  }
  return next;
}

std::vector<int> CMatrixGraph::GetPrevVertices(int vertex) const {
  std::vector<int> prev;
  for (int i = 0; i < matrix_.size(); i++) {
    if (matrix_[i][vertex] == true ) {
      prev.push_back(i);
    }
  }
  return prev;
}
