#pragma once

#include <iostream>
#include <unordered_set>

#include "igraph.hpp"

class CSetGraph : public IGraph {
 public:
  explicit CSetGraph(size_t vertices_count);
  CSetGraph(IGraph* graph);

  void AddEdge(int from, int to) override;

  int VerticesCount() const override { return array_.size(); }

  std::vector<int> GetNextVertices(int vertex) const override;
  std::vector<int> GetPrevVertices(int vertex) const override;

 private:
  std::vector<std::unordered_set<int>> array_;
};
