#pragma once

#include <iostream>

#include "igraph.hpp"

class CMatrixGraph : public IGraph {
 public:
  explicit CMatrixGraph(size_t vertices_count);
  CMatrixGraph(IGraph* graph);

  void AddEdge(int from, int to) override;

  int VerticesCount() const override { return matrix_.size(); }

  std::vector<int> GetNextVertices(int vertex) const override;
  std::vector<int> GetPrevVertices(int vertex) const override;

 private:
  std::vector<std::vector<bool>> matrix_;
};
