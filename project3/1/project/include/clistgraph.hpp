#pragma once

#include <iostream>

#include "igraph.hpp"

class CListGraph : public IGraph {
 public:
  explicit CListGraph(size_t vertices_count);
  CListGraph(IGraph* graph);

  void AddEdge(int from, int to) override;

  int VerticesCount() const override { return out_edges_.size(); }

  std::vector<int> GetNextVertices(int vertex) const override;
  std::vector<int> GetPrevVertices(int vertex) const override;

 private:
  std::vector<std::vector<int>> out_edges_;
  std::vector<std::vector<int>> in_edges_;
};
