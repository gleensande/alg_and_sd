#include <iostream>
#include <cassert>

using std::cin;
using std::cout;
using std::endl;
using std::max;

int defaultInitialSize = 16;

struct Point {
 private:
	int x;
	int y;
 public:
	Point() : x(0), y(0) {}
	Point(int _x, int _y) : x(_x), y(_y) {}
	int getX() { return x; }
	int getY() { return y; }
};

class DynamicArr {
 private:
	Point* buffer;
	int bufferSize;
	int realSize;
	void grow();
 public:
	DynamicArr() : buffer(0), bufferSize(0), realSize(0) {}
	~DynamicArr() { delete[] buffer; }
	int size() const { return realSize; }
	Point getAt(int index) const;
	Point operator[] (int index) const { return getAt(index); }
	void add(Point element);
};

Point DynamicArr::getAt(int index) const {
	assert(index >= 0 && index < realSize && buffer != 0);
	return buffer[index];
}

void DynamicArr::grow() {
	int newBufferSize = max(2 * bufferSize, defaultInitialSize);
	Point* newBuffer = new Point[newBufferSize];
	for (int i = 0; i < realSize; ++i) {
		newBuffer[i] = buffer[i];
	}
	delete[] buffer;
	buffer = newBuffer;
	bufferSize = newBufferSize;
}

void DynamicArr::add(Point element) {
	if(bufferSize == realSize) {
		grow();
	}
	assert(realSize < bufferSize && buffer != 0);
	buffer[realSize] = element;
	realSize++;
}

int main() {
	int n = 0;
	cin >> n;

	DynamicArr points;

	int x = 0, y = 0;
	for (int i = 0; i < n; i++) {
		cin >> x >> y;
		points.add(Point(x, y));
	}

	int size = points.size();
	double s = 0;
	Point now;
	Point next;

	for (int i = 0; i < size; i++) {
		now = points.getAt(i);
		if (i != size - 1) {
			next = points.getAt(i + 1);
		} else {
			next = points.getAt(0);
		}
		s += static_cast<double>((next.getY() + now.getY()) * (next.getX() - now.getX())) / 2;
	}

	cout << s << endl;

	return 0;
}
