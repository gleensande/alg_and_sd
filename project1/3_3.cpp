#include <iostream>
#include <cassert>

#define POP_FRONT 2
#define PUSH_BACK 3

using std::cin;
using std::cout;
using std::endl;
using std::max;

int defaultInitialSize = 16;

class Stack {
 private:
	int* buffer;
	int bufferSize;
	int realSize;
	void grow();
 public:
	Stack() : buffer(0), bufferSize(0), realSize(0) {}
	~Stack() { delete[] buffer; }
	int size() const { return realSize; }
	void push(int element);
	int pop();
	bool isEmpty() const { return (realSize == 0 ? 1 : 0);}
	void print() const;
};

void Stack::grow() {
	int newBufferSize = max(2 * bufferSize, defaultInitialSize);
	int* newBuffer = new int[newBufferSize];
	for (int i = 0; i < realSize; ++i) {
		newBuffer[i] = buffer[i];
	}
	delete[] buffer;
	buffer = newBuffer;
	bufferSize = newBufferSize;
}

void Stack::push(int element) {
	if(bufferSize == realSize) {
		grow();
	}
	assert(realSize < bufferSize && buffer != 0);
	buffer[realSize] = element;
	realSize++;
}

int Stack::pop() {
	if(isEmpty()) {
		return -1;
	}
	realSize--;
	return buffer[realSize];
}

void Stack::print() const {
	if (bufferSize == 0 || buffer == 0 || realSize == 0) {
		return;
	}
	for (int i = 0; i < realSize; i++) {
		cout << buffer[i] << ' ';
	}
	cout << endl;
}

void reload(Stack* Push_st, Stack* Pop_st) {
	while(!(Push_st->isEmpty())) {
		Pop_st->push(Push_st->pop());
	}
}

class Queue {
 private:
	Stack Push_st;
	Stack Pop_st;
 public:
	void push(int x);
	int pop(void);
};

void Queue::push(int x) {
	Push_st.push(x);
}

int Queue::pop(void) {
	if (Pop_st.isEmpty()) {
		reload(&Push_st, &Pop_st);
	}
	int popped = Pop_st.pop();
	return popped;
}

int main() {
	int n;
	cin >> n;
	int a, b;
	int popped, answer = 1;
	Queue queue;
	for (int i = 0; i < n; i++) {
		cin >> a;
		cin >> b;
		switch (a) {
			case (PUSH_BACK): 	queue.push(b); break;
			case (POP_FRONT):	popped = queue.pop();
								if (popped != b) {
									answer = 0;
								}
								break;
		}
	}

	cout << (answer ? "YES" : "NO") << endl;
	return 0;
}
