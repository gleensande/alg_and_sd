#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <forward_list>

using std::cout;
using std::cin;
using std::cerr;
using std::string;
using std::endl;
using std::forward_list;
using std::next;
using std::vector;

class HashTable { 
 public:
	explicit HashTable(size_t size);
	HashTable(const HashTable&) = delete;
	HashTable& operator=(HashTable&&) = delete;

	bool Has(const string& key) const;
	bool Add(const string& key);
	bool Remove(const string& key);
 private:
	vector<forward_list<string>> table;
};

int Hash(const string& key) {
	if (key.empty()) {
		return 3;
	}
	return key[0];
}

HashTable::HashTable(size_t size) :
	table(size)
{}

bool HashTable::Has (const string& key) const {
	size_t hash = Hash(key) % table.size();
	const auto& list = table[hash];
	return find(list.begin(), list.end(), key) != list.end();
}

bool HashTable::Add (const string& key) {
	size_t hash = Hash(key) % table.size();
	auto& list = table[hash];
	if (find(list.begin(), list.end(), key) != list.end()) {
		return false;
	}
	list.emplace_front(key);
	return true;
}

bool HashTable::Remove (const string& key) {
	size_t hash = Hash(key) % table.size();
	auto& list = table[hash];
	if (list.empty()) {
		return false;
	}
	if (list.front() == key) {
		list.pop_front();
		return true;
	}

	// Итерируемся по списку, проверяя следующий элемент за текущим
	for (auto it = list.begin(); next(it) != list.end(); ++it) {
		if (*next(it) == key) {
			// Удаляем следующий за текущим
			list.erase_after(it);
			return true;
		}	
	}

	return false;
}

int main() {
	HashTable strings(80);
	char command;
	string key;
	while (cin >> command >> key) {
		switch(command) {
			case '?':
				cout << (strings.Has(key) ? "OK" : "FAIL") << endl;
				break;
			case '+':
				cout << (strings.Add(key) ? "OK" : "FAIL") << endl;
				break;
			case '-':
				cout << (strings.Remove(key) ? "OK" : "FAIL") << endl;
				break;
			default:
				cerr << "bad input" << endl;
				return 0;
		}
	}


	return 0;
}
