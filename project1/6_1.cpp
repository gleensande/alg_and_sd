#include <iostream>
#include <cassert>
#include <cstring>

using std::cin;
using std::cout;
using std::endl;
using std::max;

int defaultInitialSize = 16;

class DynamicArr {
 private:
	int* buffer;
	int bufferSize;
	int realSize;
	void grow();
 public:
	DynamicArr() : buffer(0), bufferSize(0), realSize(0) {}
	~DynamicArr() { delete[] buffer; }
	int size() const { return realSize; }
	int getAt(int index) const;
	int operator[] (int index) const { return getAt(index); }
	void add(int element);
	int findMed(int left, int right);
	void swap(int i, int j);
	int Partition(int left, int right);
	int KStat(int k);
	void print() const;
};

int DynamicArr::getAt(int index) const {
	assert(index >= 0 && index < realSize && buffer != 0);
	return buffer[index];
}

void DynamicArr::grow() {
	int newBufferSize = max(2 * bufferSize, defaultInitialSize);
	int* newBuffer = new int[newBufferSize];
	for (int i = 0; i < realSize; ++i) {
		newBuffer[i] = buffer[i];
	}
	delete[] buffer;
	buffer = newBuffer;
	bufferSize = newBufferSize;
}

void DynamicArr::add(int element) {
	if(bufferSize == realSize) {
		grow();
	}
	assert(realSize < bufferSize && buffer != 0);
	buffer[realSize] = element;
	realSize++;
}

void DynamicArr::swap(int i, int j) {
	int swp = buffer[i];
	buffer[i] = buffer[j];
	buffer[j] = swp;
}
		
int DynamicArr::findMed(int left, int right) {
	int mid = (left + right) / 2;
	
	if (buffer[left] > buffer[right] && buffer[left] > buffer[mid]) {
		return (buffer[right] > buffer[mid] ? right : mid);
	}
	
	if (buffer[right] > buffer[left] && buffer[right] > buffer[mid]) {
		return (buffer[left] > buffer[mid] ? left : mid);
	}

	return (buffer[right] > buffer[left] ? right : left);
}

int DynamicArr::Partition(int left, int right) {
	int pivot_i = findMed(left, right);

	swap(pivot_i, right);
	pivot_i = right;

	int pivot = buffer[pivot_i];

	int i = left;
	int j = left;
	while (j != pivot_i) {
		if (buffer[j] > pivot) {
			j++;
		} else {
			swap(i, j);
			i++;
			j++;
		}
	}
	swap(i, pivot_i);
	return i;
}

int DynamicArr::KStat(int k) {
	int left = 0;
	int right = realSize - 1;

	int	pivot_i = Partition(left, right);
	while (pivot_i != k) {
		if (pivot_i > k) {
			right = pivot_i - 1;
		} else if (pivot_i < k) {
			left = pivot_i + 1;
		}
		pivot_i = Partition(left, right);
	}
	return buffer[k];
}

void DynamicArr::print() const {
	if (bufferSize == 0 || buffer == 0 || realSize == 0) {
		cout << "[empty]";
	}
	for (int i = 0; i < realSize; i++) {
		cout << buffer[i] << ' ';
	}
	cout << endl;
}

int main() {
	int n, k;
	cin >> n >> k;

	DynamicArr numbers;

	int inp;
	for (int i = 0; i < n; i++) {
		cin >> inp;
	   	numbers.add(inp);
	}	

	cout << numbers.KStat(k) << endl;

	return 0;
}
