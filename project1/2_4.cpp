#include <iostream>
#include <cassert>

using std::cin;
using std::cout;
using std::endl;
using std::max;

#define ABS(a) (((a) >= 0) ? (a) : -(a))

int defaultInitialSize = 16;

class DynamicArr {
 private:
	int* buffer;
	int bufferSize;
	int realSize;
	void grow();
 public:
	DynamicArr() : buffer(0), bufferSize(0), realSize(0) {}
	~DynamicArr() { delete[] buffer; }
	int size() const { return realSize; }
	int getAt(int index) const;
	int operator[] (int index) const { return getAt(index); }
	void add(int element);
	void print() const;
};

int DynamicArr::getAt(int index) const {
	assert(index >= 0 && index < realSize && buffer != 0);
	return buffer[index];
}

void DynamicArr::grow() {
	int newBufferSize = max(2 * bufferSize, defaultInitialSize);
	int* newBuffer = new int[newBufferSize];
	for (int i = 0; i < realSize; ++i) {
		newBuffer[i] = buffer[i];
	}
	delete[] buffer;
	buffer = newBuffer;
	bufferSize = newBufferSize;
}

void DynamicArr::add(int element) {
	if(bufferSize == realSize) {
		grow();
	}
	assert(realSize < bufferSize && buffer != 0);
	buffer[realSize] = element;
	realSize++;
}

void DynamicArr::print() const {
	assert(buffer != 0);
	for (int i = 0; i < realSize; i++) {
		cout << buffer[i] << ' ';
	}
	cout << endl;
}

DynamicArr* find_twin(const DynamicArr* to, const DynamicArr* in);
int binary_search_twin(const DynamicArr* in, const int el);
int diff(int x, int y);

int main() {
	int n = 0;
	cin >> n;

	DynamicArr* arrN = new DynamicArr;

	int x = 0;
	for (int i = 0; i < n; i++) {
		cin >> x;
		arrN->add(x);
	}

	int m = 0;
	cin >> m;

	DynamicArr* arrM = new DynamicArr;

	for (int i = 0; i < m; i++) {
		cin >> x;
		arrM->add(x);
	}

	DynamicArr* result = find_twin(arrM, arrN);
	result->print();

	delete result;
	delete arrM;
	delete arrN;
	return 0;
}

DynamicArr* find_twin(const DynamicArr* to, const DynamicArr* in) {
	DynamicArr* out = new DynamicArr;
	int x = 0, y = 0;

	for (int i = 0; i < to->size(); i++) {
		x = to->getAt(i);
		y = binary_search_twin(in, x);
		out->add(y);
	}

	return out;
}

int binary_search_twin(const DynamicArr* in, const int el) {
	int i = 0;
	int next_i = 1;
	while ((next_i < in->size()) && (el > in->getAt(next_i))) {
		i = next_i;
		next_i *= 2;
	}

	int first = i;
	int last = (in->size() > (next_i + 1) ? next_i + 1 : in->size());
	int min_diff_index = i;
	int b_mid_el, mid_el, a_mid_el;  // mid = index, b = before, a = after, el = element
	int mid = (first + last) / 2;

	while (first < last && mid != 0) {
		mid_el = in->getAt(mid);
		if (mid != 0) {
			b_mid_el = in->getAt(mid - 1);
		}
		if (mid != last - 1) {
			a_mid_el = in->getAt(mid + 1);
		}

		if (mid_el != el) {  // пока элемент меньше, удваиваем индекс
			if (el < mid_el) {
				if (el >= b_mid_el) {
					if (diff(el, mid_el) >= diff(el, b_mid_el)) {
						min_diff_index = mid - 1;
						first = last;
					} else {
						min_diff_index = mid;
						first = last;
					}
				} else {
					if (mid - 1 != first) {
						last = mid;
					} else {
						min_diff_index = first;
						first = last;
					}
				}
			} else {
				if (el < a_mid_el) {
					if (diff(el, mid_el) <= diff(el, a_mid_el)) {
						min_diff_index = mid;
						first = last;
					} else {
						min_diff_index = mid + 1;
						first = last;
					}
				} else {
					if (mid != last - 1) {
						first = mid + 1;
					} else {
						min_diff_index = last - 1;
						first = last;
					}
				}
			}
		} else {
			min_diff_index = mid;
			first = last;
		}

		mid = (first + last) / 2;
	}

	return min_diff_index;
}

int diff(int x, int y) {
	return ABS(x - y);
}

